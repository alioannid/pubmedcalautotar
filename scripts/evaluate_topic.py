# author = Alexandros Ioannidis
import lightgbm as lgb
from sklearn import svm
from sklearn.linear_model import SGDClassifier
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
from xgboost import XGBClassifier
from rlscore.learner import CGRLS
from rlscore.measure import auc
import numpy as np



def evaluate_topic(X_train, y_train, X_test, classifier):

  if classifier == 'svm':

    
    # Create a linear SVM classifier
    # subsample the training samples of one of my categories because I want a classifier that treats all the labels the same. 
    # instead of subsampling I change the value of the 'class_weight' parameter of my classifier to 'balanced'
    clf = svm.SVC(kernel='linear', class_weight='balanced',
                  probability=True, random_state=848)
    
    #kernels = ['linear', 'rbf', 'poly']
    #clf = svm.SVC(kernel='rbf')
    '''
    clf = svm.SVC(C=1.0, kernel='linear', degree=3,
                  probability=True, gamma='auto')
    '''
    clf.fit(X_train, y_train)

    # the correct is with 1 apparently
    y_test = clf.predict_proba(X_test)[:, 1]
    #y_test = clf.predict_proba(X_test)[:, 0]
    
    return(y_test)

  elif classifier == 'lgb':
    
    param = {'num_leaves': 31, 'num_iterations': 100, 'max_depth': -1, 'objective': 'binary',
             'is_unbalance': True, 'metric': ['l2', 'binary_logloss'], 'verbose': -1}
    '''
    param = {'num_leaves': 31, 'num_iterations': 100, 'max_depth': -1, 'objective': 'binary', 'is_unbalance': False, 'metric': ['l2','binary_logloss'], 'verbose': -1 }

    param = { 'boosting_type': 'gbdt', 'objective': 'binary', 'metric': 'binary_logloss', 'num_leaves': 31, 'learning_rate': 0.05, 'feature_fraction': 0.9, 'bagging_fraction': 0.8, 'bagging_freq': 5, 'verbose': 0  }
    '''

    train_data = lgb.Dataset(X_train, label=y_train)

    clf = lgb.train(param, train_data)  

    y_test = clf.predict(X_test)  # [:,1]
    #print(y_test)

    return(y_test)

  elif classifier == 'sgd':
    
    clf = SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, random_state=42, max_iter=5, tol=None)

    clf.fit(X_train, y_train)

    y_test = clf.predict(X_test)

    return(y_test)

  elif classifier == 'xgb':
    
    clf = XGBClassifier()
    
    clf.fit(X_train, y_train)

    y_test = clf.predict(X_test)

    return(y_test)

  # CGRLS does not support multi-output learning, so we train one classifier for the first column of Y. Multi-class learning 
  # would be implemented by training one CGRLS for each column, and taking the argmax of class predictions.
  elif classifier == 'rls':

    #clf = CGRLS(X_train, Y_train[:, 0], regparam=100.0)
    clf = CGRLS(X_train, y_train, regparam=100.0)

    y_test = clf.predict(X_test)

    return(y_test)

  elif classifier == 'dtc':

    clf = DecisionTreeClassifier(random_state=0)

    clf.fit(X_train, y_train)
    
    y_test = clf.predict(X_test)

    return(y_test)

  elif classifier == 'rfc':

    clf = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)

    clf.fit(X_train, y_train)

    y_test = clf.predict(X_test)

    return(y_test)

  elif classifier == 'kne':
    
    clf = KNeighborsClassifier(n_neighbors=3)

    clf = clf.fit(X_train, y_train)

    y_test = clf.predict(X_test)

    return(y_test)     

  elif classifier == 'smote':
    smt = SMOTE()
    smt = SMOTE(sampling_strategy='auto')
    X_train_sampled, y_train_sampled = smt.fit_sample(X_train.toarray(), np.asarray(y_train))
    clf = LinearSVC().fit(X_train_sampled, y_train_sampled)
    y_test = clf.predict(X_test) # maybe try putting X_test in [] like this [X_test]
    return(y_test)

  elif classifier == 'lr':
    
    clf = LogisticRegression()
    
    clf.fit(X_train, y_train)
    
    y_test = clf.predict(X_test)

    return(y_test)
  
  else:

    raise Exception('Wrong classifier')

  return(y_test)
